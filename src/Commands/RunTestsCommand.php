<?php

namespace TekoEstudio\ApiTesting\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TekoEstudio\ApiTesting\ApiTesting;
use TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException;
use TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException;
use TekoEstudio\ApiTesting\Handler\HandleTests;
use TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog;
use TekoEstudio\ApiTesting\Handler\Schemes\TestCaseResultsCollectionSchemes;
use TekoEstudio\ApiTesting\Results\TestCaseResult;
use Throwable;

class RunTestsCommand extends Command
{
    /**
     * Command name
     *
     * @var string
     */
    protected static $defaultName = 'tests:run';

    /**
     * Command description
     *
     * @var string
     */
    protected static $defaultDescription = 'Run tests synchronously by group';

    /**
     * Code at colors for outputs
     *
     * @var array $colors
     */
    private array $colors = array(
        'red'     => "\e[31m",
        'green'   => "\e[32m",
        'yellow'  => "\e[33m",
        'blue'    => "\e[34m",
        'default' => "\e[39m"
    );

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->addOption('group', 'g', InputOption::VALUE_REQUIRED,
            'Group of tests for handle'
        );

        $this->addOption('fresh', null, InputOption::VALUE_OPTIONAL,
            'Clear all logs', false
        );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $group      = $input->getOption('group');
            $clearLogs  = $input->getOption('fresh');
            $apiHandler = ApiTesting::handle($group, $clearLogs);

            $output->writeln('Tests ready...');
            $this->renderPreparationTable($apiHandler, $output);
            $apiHandler->handleAll();

            $testsCasesResultsCollections = $apiHandler->getTestCasesResultsCollections();

            $output->writeln(PHP_EOL . $this->colors['green'] . 'All tests finished!' . $this->colors['default']);
            $output->writeln(PHP_EOL . $this->colors['yellow'] . 'Logs folder: ' .
                             $apiHandler->errorsLogger->id . $this->colors['default']
            );

            $this->renderTestsCasesResultsTable($testsCasesResultsCollections, $output);
            $output->writeln(PHP_EOL . 'Teko Api Testing v1.0.0' . PHP_EOL . 'Tkila Framework');

            return self::SUCCESS;

        } catch (TestsGroupsNotFoundException) {

            $output->writeln('Tests group not found, please check the tests slug entered');
            return self::INVALID;

        } catch (TestGroupInstanceInvalidException) {

            $output->writeln('The instance call for tests groups is invalid');
            return self::INVALID;

        } catch (Throwable $th) {

            $output->writeln('An error occurred while api testing library is handled');
            $output->writeln($th->getMessage());
            var_dump($th);

            return self::FAILURE;
        }
    }

    /**
     * @param \TekoEstudio\ApiTesting\Handler\HandleTests       $apiHandler
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    private function renderPreparationTable(HandleTests $apiHandler, OutputInterface $output): void
    {
        $preparationTable = new Table($output);
        $rows             = array();

        $rows[] = ['Tests prepared for execute', $apiHandler->getPreparedTestsNum()];
        $rows[] = ['Tests failed in preparation for execute', $apiHandler->getUnpreparedTestsNum()];

        $preparationTable->setHeaders(['Description', 'Total']);
        $preparationTable->setRows($rows);
        $preparationTable->render();
    }

    /**
     * @param array                                             $testCasesResultsCollections
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    private function renderTestsCasesResultsTable(array $testCasesResultsCollections, OutputInterface $output): void
    {
        $preparationTable = new Table($output);
        $rows             = array();
        $dumps            = array();

        foreach ($testCasesResultsCollections as $testCaseResult) {
            $results = [$testCaseResult->groupName, '', '', '', ''];

            if ($testCaseResult instanceof TestCaseResultsCollectionSchemes) {
                foreach ($testCaseResult->getResults() as $result) {
                    if ($result instanceof TestCaseResult) {
                        $results[1] .= $result->testCaseName . PHP_EOL;
                        $results[2] .= ($result->isPass() ? $this->getOkIcon() : $this->getXIcon()) . PHP_EOL;
                        $results[3] .= (!$result->isPass() ? $this->shortMessage($result->getException()) : '') . PHP_EOL;
                        $results[4] .= (!$result->isPass() ? $this->getErrorLogLink($result->getErrorLog()) : '') . PHP_EOL;
                    }
                }
            }
            $rows[] = $results;
        }

        $preparationTable->setHeaders(['Group name', 'Use case name', 'Pass', 'Details', 'Log']);
        $preparationTable->setRows($rows);
        $preparationTable->render();
    }

    /**
     * @return string
     */
    private function getOkIcon(): string
    {
        return $this->colors['green'] . '✔' . $this->colors['default'];
    }

    /**
     * @return string
     */
    private function getXIcon(): string
    {
        return $this->colors['red'] . 'x' . $this->colors['default'];
    }

    /**
     * @param \Throwable $throwable
     *
     * @return string
     */
    private function shortMessage(Throwable $throwable): string
    {
        $message = $throwable->getMessage();

        return (strlen($message) > 30) ?
            substr($message, 0, 30) . '...' :
            $message;
    }

    /**
     * @param \TekoEstudio\ApiTesting\Handler\Schemes\TestCaseErrorLog $caseErrorLog
     *
     * @return string
     */
    private function getErrorLogLink(TestCaseErrorLog $caseErrorLog): string
    {
        return $caseErrorLog->id;
    }
}