<?php

namespace TekoEstudio\ApiTesting\Environments;

use Exception;
use TekoEstudio\ApiTesting\Schemes\Environment;
use TekoEstudio\ApiTesting\Store\TestStorePersistent;
use TekoEstudio\ApiTesting\Traits\StorePersistent;

final class TestEnvironment
{
    private static ?Environment $instance = null;

    /**
     * @return \TekoEstudio\ApiTesting\Schemes\Environment
     */
    public static function getInstance(): Environment
    {
        if (TestEnvironment::$instance === null) {
            TestEnvironment::$instance                  = new Environment();
            TestEnvironment::$instance->storePersistent = new TestStorePersistent();
        }

        return TestEnvironment::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct()
    {
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone()
    {
    }

    /**
     * prevent from being unserialized (which would create a second instance of it)
     * @throws \Exception
     */
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }
}