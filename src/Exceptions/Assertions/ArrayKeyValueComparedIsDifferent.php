<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;

class ArrayKeyValueComparedIsDifferent extends AssertException
{
    /**
     * @param string $key
     */
    #[Pure]
    public function __construct(string $key)
    {
        parent::__construct("Key $key value is different than expected");
    }
}