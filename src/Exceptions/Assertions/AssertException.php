<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Exceptions\TestException;
use Throwable;

class AssertException extends TestException
{
    #[Pure]
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}