<?php

namespace TekoEstudio\ApiTesting\Exceptions\Assertions;

use JetBrains\PhpStorm\Pure;

class JsonResultIsDifferentException extends AssertException
{
    #[Pure]
    public function __construct()
    {
        parent::__construct('The request json is different that excepted');
    }
}