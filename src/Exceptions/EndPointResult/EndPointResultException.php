<?php

namespace TekoEstudio\ApiTesting\Exceptions\EndPointResult;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Exceptions\TestException;
use Throwable;

class EndPointResultException extends TestException
{
    /**
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    #[Pure]
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}