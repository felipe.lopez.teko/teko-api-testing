<?php

namespace TekoEstudio\ApiTesting\Exceptions\EndPointResult;

use JetBrains\PhpStorm\Pure;

class NotRequestAvailableException extends EndPointResultException
{
    /**
     * Exception constructor.
     */
    #[Pure] public function __construct()
    {
        parent::__construct('The request in test case result is failed');
    }
}