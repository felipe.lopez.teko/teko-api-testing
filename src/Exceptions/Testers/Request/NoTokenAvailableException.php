<?php

namespace TekoEstudio\ApiTesting\Exceptions\Testers\Request;

use JetBrains\PhpStorm\Pure;

class NoTokenAvailableException extends TesterRequestException
{
    /**
     * Constructor exception
     */
    #[Pure]
    public function __construct()
    {
        parent::__construct('No token available in bearer token header for request');
    }
}