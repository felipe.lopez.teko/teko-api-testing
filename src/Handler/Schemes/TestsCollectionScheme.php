<?php

namespace TekoEstudio\ApiTesting\Handler\Schemes;

use JetBrains\PhpStorm\Pure;

class TestsCollectionScheme
{
    /**
     * @var array
     */
    private array $testsSchemes = array();

    /**
     * @var int
     */
    protected int $incompatibleTests = 0;

    /**
     * @var int
     */
    private int $index = 0;

    /**
     * Constructor.
     */
    public function __construct()
    {
        // TODO.
    }

    /**
     * Add new test
     *
     * @param \TekoEstudio\ApiTesting\Handler\Schemes\TestScheme $scheme
     *
     * @return void
     */
    public function add(TestScheme $scheme): void
    {
        $this->testsSchemes[] = $scheme;
    }

    /**
     * Get test by index
     *
     * @param int $index
     *
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestScheme
     */
    private function get(int $index): TestScheme
    {
        return $this->testsSchemes[$index];
    }

    /**
     * Increment counter for incompatible tests for run
     *
     * @return void
     */
    public function incrementIncompatibleTests(): void
    {
        $this->incompatibleTests++;
    }

    /**
     * Get incompatible tests number
     *
     * @return int
     */
    public function getIncompatibleTestsNum(): int
    {
        return $this->incompatibleTests;
    }

    /**
     * Get executable test counter number
     *
     * @return int
     */
    public function getExecutableTestsNum(): int
    {
        return count($this->testsSchemes);
    }

    /**
     * Verify if it has a next test for run
     *
     * @return bool
     */
    #[Pure]
    public function isIterable(): bool
    {
        return $this->getExecutableTestsNum() > $this->index;
    }

    /**
     * Get next test in array list
     *
     * @return \TekoEstudio\ApiTesting\Handler\Schemes\TestScheme|null
     */
    public function next(): ?TestScheme
    {
        if (!$this->isIterable()) {
            return null;
        }

        $scheme = $this->get($this->index);
        $this->index++;

        return $scheme;
    }
}