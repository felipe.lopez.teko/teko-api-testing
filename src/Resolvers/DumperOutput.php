<?php

namespace TekoEstudio\ApiTesting\Resolvers;

use Symfony\Component\VarDumper\VarDumper;
use TekoEstudio\ApiTesting\Resolvers\ConsoleOutput\ConsoleColorsCode;

trait DumperOutput
{
    /**
     * @param mixed $var
     *
     * @return \TekoEstudio\ApiTesting\Resolvers\DumperOutput
     */
    public function dumper(mixed $var): static
    {
        print($this->getSeparator() . 'Dumping info from test' . $this->getSeparator());
        VarDumper::dump($var);
        print(ConsoleColorsCode::Default->value . $this->getSeparator());
        return $this;
    }

    /**
     * @return string
     */
    private function getSeparator(): string
    {
        return PHP_EOL . str_repeat('-', 35) . PHP_EOL;
    }
}