<?php

namespace TekoEstudio\ApiTesting\Schemes;

use Exception;
use TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException;
use TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException;
use TekoEstudio\ApiTesting\TestCases\TestGroup;
use Throwable;

abstract class TestGroupLoggerPrepare
{
    /**
     * @var array|string[]
     */
    public array $testsGroupNames = [];

    /**
     * @param string $testGroup
     *
     * @return bool
     */
    public function testGroupExists(string $testGroup): bool
    {
        return isset($this->testsGroupNames[$testGroup]);
    }

    /**
     * @param string $testGroup
     *
     * @return \TekoEstudio\ApiTesting\TestCases\TestGroup
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestGroupInstanceInvalidException
     * @throws \TekoEstudio\ApiTesting\Exceptions\TestsGroups\TestsGroupsNotFoundException
     */
    public function getTestInstanceByGroup(string $testGroup): TestGroup
    {
        if (!$this->testGroupExists($testGroup)) {
            throw new TestsGroupsNotFoundException();
        }

        try {
            $testGroupInstance = new $this->testsGroupNames[$testGroup]();

            if ($testGroupInstance instanceof TestGroup) {
                $testGroupInstance->boot();
                return $testGroupInstance;
            }

            throw new Exception();

        } catch (Throwable) {
            throw new TestGroupInstanceInvalidException();
        }
    }
}