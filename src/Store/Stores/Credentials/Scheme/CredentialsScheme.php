<?php

namespace TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme;

class CredentialsScheme
{
    /**
     * @param string $slug
     * @param string $username
     * @param string $password
     * @param string $token
     */
    public function __construct(
        public string $slug,
        public string $username = '',
        public string $password = '',
        public string $token = ''
    )
    {
        // TODO.
    }
}