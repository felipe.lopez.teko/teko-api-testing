<?php

namespace TekoEstudio\ApiTesting\TestCases;

abstract class AbstractTestCase
{
    /**
     * Tests methods names
     *
     * @var array|null
     */
    private ?array $testsNames = null;

    /**
     * Exclude default method inside library
     *
     * @var array
     */
    private array $excludeDefaults = [
        'tester'
    ];

    /**
     * @return array
     */
    public function getTestsNames(): array
    {
        if (is_null($this->testsNames)) {
            $this->testsNames = [];
            foreach (get_class_methods($this) as $methodName) {
                if (str_starts_with($methodName, 'test') && !in_array($methodName, $this->excludeDefaults)) {
                    $this->testsNames[] = $methodName;
                }
            }
        }

        return $this->testsNames;
    }

    public function getta()
    {
        return get_parent_class($this);
    }
}