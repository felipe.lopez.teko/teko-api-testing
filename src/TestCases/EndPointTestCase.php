<?php

namespace TekoEstudio\ApiTesting\TestCases;

use TekoEstudio\ApiTesting\Results\Types\TestTypes;
use TekoEstudio\ApiTesting\Testers\Requests\TestRequest;

abstract class EndPointTestCase extends TestCase
{
    /**
     * Route for send requests in tester
     *
     * @var string
     */
    protected string $route = '';

    /**
     * Uri path to request
     *
     * @var string
     */
    protected string $uri = '';

    /**
     * Test case type
     *
     * @var \TekoEstudio\ApiTesting\Results\Types\TestTypes
     */
    public TestTypes $type = TestTypes::EndPoint;

    /**
     * Test constructor
     */
    abstract public function __construct();

    /**
     * Prepare config for execute test
     *
     * @return void
     */
    public function prepare(): void
    {
        // TODO.
    }

    /**
     * @return \TekoEstudio\ApiTesting\Testers\Requests\TestRequest
     */
    protected function request(): TestRequest
    {
        $testRequest = new TestRequest();

        // Set route
        if (!empty($this->route)) {
            $testRequest->setBaseUri($this->route);
        }

        // Set uri
        if (!empty($this->uri)) {
            $testRequest->setRoute($this->uri);
        }

        return $testRequest->self();
    }
}