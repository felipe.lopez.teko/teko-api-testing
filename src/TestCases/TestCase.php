<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\TestCases;

use TekoEstudio\ApiTesting\Resolvers\DumperOutput;
use TekoEstudio\ApiTesting\Results\Types\TestTypes;
use TekoEstudio\ApiTesting\Traits\Assert;
use TekoEstudio\ApiTesting\Traits\Faker;
use TekoEstudio\ApiTesting\Traits\StorePersistent;

class TestCase extends AbstractTestCase
{
    use StorePersistent, Faker, Assert, DumperOutput;

    /**
     * Test case type
     *
     * @var \TekoEstudio\ApiTesting\Results\Types\TestTypes
     */
    public TestTypes $type;

    /**
     * @return string
     */
    public function getTestGroupName(): string
    {
        $parentClassName = get_parent_class($this);
        $nameSplit       = explode("\\", $parentClassName);
        $lastKey         = array_key_last($nameSplit);

        return $nameSplit[$lastKey];
    }
}