<?php

namespace TekoEstudio\ApiTesting\Testers\Requests\Http;

use TekoEstudio\ApiTesting\Environments\TestEnvironment;

class HttpMethods extends HttpOptions
{
    /**
     * @var string|null
     */
    private ?string $baseUri = null;

    /**
     * @var string
     */
    private string $uri = '';

    /**
     * @var \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpTypeMethods
     */
    protected HttpTypeMethods $method;

    /**
     * Set base uri
     *
     * @param string $uri
     *
     * @return \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpMethods
     */
    public function setBaseUri(string $uri): static
    {
        $this->baseUri = $uri;
        return $this;
    }

    /**
     * Set uri for send request
     *
     * @param string $uri
     *
     * @return \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpMethods
     */
    public function setRoute(string $uri): static
    {
        if (!empty($uri)) {
            filter_var($uri, FILTER_VALIDATE_URL) ? $this->setBaseUri($uri) : $this->uri = $uri;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseUri(): string
    {
        return $this->baseUri ?? TestEnvironment::getInstance()?->getEndPointEnvironment()?->route ?? '';
    }

    /**
     * @return string
     */
    public function getRouteUri(): string
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method->name;
    }

    /**
     * @param string $uri
     *
     * @return \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpMethods
     */
    public function get(string $uri = ''): static
    {
        $this->setRoute($uri);
        $this->method = HttpTypeMethods::GET;

        return $this;
    }

    /**
     * Send POST method request
     *
     * @param string $uri
     *
     * @return \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpMethods
     */
    public function post(string $uri = ''): static
    {
        $this->setRoute($uri);
        $this->method = HttpTypeMethods::POST;

        return $this;
    }

    /**
     * Send PUT method request
     *
     * @param string $uri
     *
     * @return \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpMethods
     */
    public function put(string $uri = ''): static
    {
        $this->setRoute($uri);
        $this->method = HttpTypeMethods::PUT;

        return $this;
    }

    /**
     * @param string $uri
     *
     * @return \TekoEstudio\ApiTesting\Testers\Requests\Http\HttpMethods
     */
    public function delete(string $uri = ''): static
    {
        $this->setRoute($uri);
        $this->method = HttpTypeMethods::DELETE;

        return $this;
    }
}