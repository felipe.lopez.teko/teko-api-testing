<?php
/** @noinspection PhpUnused */

namespace TekoEstudio\ApiTesting\Testers\Requests\Http;

use GuzzleHttp\Psr7\Utils;
use TekoEstudio\ApiTesting\Exceptions\Testers\Request\NoTokenAvailableException;
use TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme\CredentialsScheme;
use TekoEstudio\ApiTesting\Traits\Faker;
use TekoEstudio\ApiTesting\Traits\StorePersistent;

class HttpOptions
{
    use StorePersistent, Faker;

    /**
     * Credential scheme for add in options
     *
     * @var \TekoEstudio\ApiTesting\Store\Stores\Credentials\Scheme\CredentialsScheme|null
     */
    private ?CredentialsScheme $credentialsScheme = null;

    /**
     * Http options for client
     *
     * @var array
     */
    protected array $options = [
        'headers' => [
            'Accept'          => 'application/json',
            'Accept-Encoding' => 'gzip, deflate, br'
        ]
    ];

    /**
     * Add custom header to request
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return static
     */
    public function addHeader(string $name, mixed $value): static
    {
        $this->options['headers'][$name] = $value;
        return $this;
    }

    /**
     * Add param to uri
     *
     * @param string     $name
     * @param string|int $value
     *
     * @return static
     */
    public function addUriParam(string $name, string|int $value): static
    {
        $this->options['query']   ??= [];
        $this->options['query'][] = [$name => $value];

        return $this;
    }

    /**
     * Add timeout limit in request
     *
     * @param float $seconds
     *
     * @return static
     */
    public function timeout(float $seconds): static
    {
        $this->options['timeout'] = $seconds;
        return $this;
    }

    /**
     * Add json to send in request
     *
     * @param array $json
     *
     * @return static
     */
    public function json(array $json): static
    {
        $this->options['json'] = $json;
        return $this;
    }

    /**
     * Add form params in request
     *
     * @param array $formParams
     *
     * @return static
     */
    public function form(array $formParams): static
    {
        $this->options['form_params'] = $formParams;
        return $this;
    }

    /**
     * Add bearer token from token string or credential scheme from store environment
     *
     * @param string|null $token
     *
     * @return static
     * @throws \TekoEstudio\ApiTesting\Exceptions\Testers\Request\NoTokenAvailableException
     */
    public function bearerToken(string $token = null): static
    {
        $bearerToken = $token ?? $this->credentialsScheme->token ?? throw new NoTokenAvailableException();

        $this->options['headers']['Authorization'] = "Bearer $bearerToken";
        return $this;
    }

    /**
     * Send file in request
     *
     * @param string $filePath
     *
     * @return static
     */
    public function file(string $filePath): static
    {
        $this->options['multipart'] ??= [];

        $file     = Utils::tryFopen($filePath, 'r');
        $tempName = $this->faker()->slug(2);

        $this->options['multipart'][] = [
            'name'     => $tempName,
            'contents' => $file
        ];

        return $this;
    }

    /**
     * Get credential from store environment
     *
     * @param string $slug
     *
     * @return static
     */
    public function credential(string $slug): static
    {
        $this->credentialsScheme = $this->store()->getCredential($slug);
        return $this;
    }

    /**
     * Add accepted language negotiation in headers
     *
     * @param string $lang
     *
     * @return static
     */
    public function language(string $lang): static
    {
        $this->addHeader('Accept-Language', $lang);
        return $this;
    }

    /**
     * Debug the request when this is sending
     *
     * @return static
     */
    public function debug(): static
    {
        $this->options['debug'] = true;
        return $this;
    }

    /**
     * Get options list for client
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return $this->options;
    }
}