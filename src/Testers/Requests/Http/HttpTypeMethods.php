<?php

namespace TekoEstudio\ApiTesting\Testers\Requests\Http;

enum HttpTypeMethods
{
    case GET;
    case POST;
    case PUT;
    case DELETE;
}