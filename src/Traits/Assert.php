<?php

namespace TekoEstudio\ApiTesting\Traits;

use JetBrains\PhpStorm\Pure;
use TekoEstudio\ApiTesting\Asserts\Asserted;

trait Assert
{
    /**
     * @return \TekoEstudio\ApiTesting\Asserts\Asserted
     */
    #[Pure]
    public function assert(): Asserted
    {
        return new Asserted();
    }
}