<?php

namespace TekoEstudio\ApiTesting\Traits;

use Faker\Factory;

trait Faker
{
    /**
     * @return \Faker\Generator
     */
    public function faker(): \Faker\Generator
    {
        return Factory::create();
    }
}