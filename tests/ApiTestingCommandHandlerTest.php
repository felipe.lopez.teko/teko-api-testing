<?php

namespace TekoEstudio\ApiTesting\Test;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use TekoEstudio\ApiTesting\Commands\RunTestsCommand;

class ApiTestingCommandHandlerTest extends TestCaseBase
{
    /**
     * @throws \Exception
     */
    public function testCommandForExecuteTests()
    {
        $input = new ArrayInput([
            '--group' => $this->testGroup,
        ]);

        $output        = new ConsoleOutput();
        $command       = new RunTestsCommand();
        $commandResult = $command->run($input, $output);

        $this->assertSame(Command::SUCCESS, $commandResult, 'The command output is not successfully');
    }

    /**
     * @throws \Exception
     */
    public function testCommandForExecuteTestsWithIncorrectGroup()
    {
        $input = new ArrayInput([
            '--group' => $this->faker->name,
        ]);

        $output        = new ConsoleOutput();
        $command       = new RunTestsCommand();
        $commandResult = $command->run($input, $output);

        $this->assertSame(Command::INVALID, $commandResult);
    }
}